using System.Collections.Generic;
using Code.Inventory;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RewardsWindow : BaseWindow
{
    [SerializeField] private Button _closeButton;
    [SerializeField] private TMP_Text _titleText;
    [SerializeField] private Transform _itemsRoot;
    [SerializeField] private RectTransform _layoutTransform;

    private const string INVENTORY_ITEM_PATH = "InventoryItem";
    
    private readonly List<InventoryItemView> _inventoryItemViews = new();
    
    protected override void Awake()
    {
        base.Awake();
        _closeButton.onClick.AddListener(Hide);
    }

    protected override void OnShow(object[] args)
    {
        _titleText.text = (string)args[0];

        var items = (List<InventoryItem>)args[1];
        
        foreach (var inventoryItem in items)
        {
            var item = Instantiate(Resources.Load<InventoryItemView>(INVENTORY_ITEM_PATH), _itemsRoot);
            item.Init(inventoryItem);
            _inventoryItemViews.Add(item);
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(_layoutTransform);
    }
    
    protected override void OnHide()
    {
        foreach (var inventoryItem in _inventoryItemViews)
            Destroy(inventoryItem.gameObject);

        _inventoryItemViews.Clear();
    }

    private void OnDestroy()
    {
        _closeButton.onClick.RemoveListener(Hide);
    }
}