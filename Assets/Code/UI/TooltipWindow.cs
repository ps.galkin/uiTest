using Code.Inventory;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TooltipWindow : BaseWindow
{
    [SerializeField] private TMP_Text _title;
    [SerializeField] private Image _icon;
    [SerializeField] private TMP_Text _description;
    [SerializeField] private TMPAreaLimiter _limiter;
    
    private const float SCALING_COEF = 0.25f;
    
    protected override void OnShow(object[] args)
    {
        _title.text = ((InventoryItem) args[0]).Title;
        _icon.sprite = ((InventoryItem) args[0]).Icon;
        _description.text = ((InventoryItem) args[0]).Description;
        
        _icon.SetNativeSize();
        var rect = _icon.rectTransform.rect;
        _icon.rectTransform.sizeDelta = new Vector2((rect.width * SCALING_COEF), (rect.height * SCALING_COEF));
        
        _limiter.LimitText();
    }

    protected override void OnHide()
    {

    }
}
