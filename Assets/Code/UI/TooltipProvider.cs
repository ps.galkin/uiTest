using Code.Inventory;
using UnityEngine;

public class TooltipProvider : MonoBehaviour
{
    [SerializeField] private RectTransform _transform;
    
    private TooltipWindow _tooltipWindow;
    private InventoryItem _inventoryItem;

    private bool _isTouching;
    private float _startTapTime;
    private const float TAP_DELAY = 2f;
    
    public void Start()
    {
        _tooltipWindow = BaseWindow.Get<TooltipWindow>();
    }
    
    private void Update()
    {
        bool touchStarted;
#if UNITY_STANDALONE_WIN
        touchStarted = Input.GetKey(KeyCode.Mouse0);
#else
        touchStarted = Input.touchCount > 0;
#endif 
        
        if (touchStarted && !_isTouching && TouchOnTooltipArea())
        {
            _isTouching = true;
            _startTapTime = Time.time;
        }
        else if (touchStarted 
            && _isTouching && Time.time > _startTapTime + TAP_DELAY 
            && !_tooltipWindow.gameObject.activeSelf)
        {
            _tooltipWindow.Show(_inventoryItem);
        }
        else if (!touchStarted && _isTouching)
        {
            _tooltipWindow.Hide();
            _isTouching = false;
        }
    }

    private bool TouchOnTooltipArea()
    {
        Vector2 position;
#if UNITY_STANDALONE_WIN
        position = Input.mousePosition;
#else 
        position = Input.GetTouch(0).position;
#endif
            
        var fourCornersArray = new Vector3[4];
        _transform.GetWorldCorners(fourCornersArray);
        var objRect = new Rect(fourCornersArray[0].x, fourCornersArray[0].y, _transform.rect.width, _transform.rect.height);

        return objRect.Contains(position);
    }
    
    public void Init(InventoryItem inventoryItem)
    {
        _inventoryItem = inventoryItem;
    }
}
