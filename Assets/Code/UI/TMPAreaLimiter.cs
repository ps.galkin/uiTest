using TMPro;
using UnityEngine;

public class TMPAreaLimiter : MonoBehaviour
{
    [SerializeField] private float minX;
    [SerializeField] private float minY;
    [SerializeField] private float maxX;
    [SerializeField] private float maxY;

    [SerializeField] private TMP_Text _text;
    [SerializeField] private RectTransform _rect;
    
    public void LimitText()
    {
        var limit = _text.GetPreferredValues(_text.text);
        if (limit.x < minX)
            limit.x = minX;
        else if (limit.x > maxX)
            limit.x = maxX;

        if (limit.y < minY)
            limit.y = minY;
        else if (limit.y > maxY)
            limit.y = maxY;
        
        _rect.sizeDelta = new Vector2(limit.x, limit.y);
    }
}
