using Code.Inventory;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItemView : MonoBehaviour
{
    [SerializeField] private Image _itemIcon;
    [SerializeField] private TooltipProvider _tooltip;

    private const float SCALING_COEF = 0.9f;

    public void Init(InventoryItem inventoryItem)
    {
        _itemIcon.sprite = inventoryItem.Icon;
        _itemIcon.SetNativeSize();
        var rect = _itemIcon.rectTransform.rect;
        _itemIcon.rectTransform.sizeDelta = new Vector2((rect.width * SCALING_COEF), (rect.height * SCALING_COEF));

        _tooltip.Init(inventoryItem);
    }
}
